/*  MELODIC - Multivariate exploratory linear optimized decomposition into
    independent components

    meloptions.h - class for command line options

    Christian F. Beckmann, FMRIB Analysis Group

    Copyright (C) 1999-2013 University of Oxford */

/*  CCOPYRIGHT  */

#ifndef __MELODICOPTIONS_h
#define __MELODICOPTIONS_h

#include <string>
#include <strstream>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include "utils/options.h"
#include "utils/log.h"
#include "melodic.h"

namespace Melodic {

  class MelodicOptions {
  public:
  	static MelodicOptions& getInstance();
  	~MelodicOptions() { delete gopt; }

    std::string version;
  	std::string binpath;
  	std::string logfname;
  	bool   filtermode;
  	bool   explicitnums;

    Utilities::Option<std::string> logdir;
  	Utilities::Option< std::vector<std::string> > inputfname;

  	Utilities::Option<std::string> outputfname;

  	Utilities::Option<std::string> maskfname;
  	Utilities::Option<bool>   use_mask;
  	Utilities::Option<bool>   update_mask;
  	Utilities::Option<bool>   perf_bet;
  	Utilities::Option<float>  threshold;

  	Utilities::Option<int>    pca_dim;
  	Utilities::Option<std::string> pca_est;
  	Utilities::Option<bool>   joined_whiten;
  	Utilities::Option<bool>   joined_vn;
	Utilities::Option<bool>   dr_pca;
	Utilities::Option<bool>   migp;
	Utilities::Option<int>    migpN;
	Utilities::Option<bool>   migp_shuffle;
	Utilities::Option<int>	   migp_factor;
	Utilities::Option<bool>   dr;
	Utilities::Option<bool>   dr_out;
	Utilities::Option<float>  vn_level;
  	Utilities::Option<int>    numICs;
  	Utilities::Option<std::string> approach;
	Utilities::Option<bool> deflation;
  	Utilities::Option<std::string> nonlinearity;

  	Utilities::Option<bool>   varnorm;
 	Utilities::Option<bool>   varnorm2;
  	Utilities::Option<bool>   pbsc;
  	Utilities::Option<bool>   pspec;
  	Utilities::Option<std::string> segment;
  	Utilities::Option<bool>   tsmooth;
  	Utilities::Option<float>  epsilon;
  	Utilities::Option<float>  epsilonS;
  	Utilities::Option<int>    maxNumItt;
  	Utilities::Option<int>    maxRestart;
  	Utilities::Option<int>    rank1interval;

  	Utilities::Option<std::string> mmthresh;
  	Utilities::Option<bool>   perf_mm;
  	Utilities::Option<std::string> ICsfname;
  	Utilities::Option<std::string> filtermix;
  	Utilities::Option<std::string> smodename;
  	Utilities::Option<std::string> filter;

  	Utilities::Option<bool>   genreport;
	Utilities::Option<std::string> guireport;
	Utilities::Option<std::string> bgimage;
  	Utilities::Option<float>  tr;
  	Utilities::Option<bool>   logPower;
	Utilities::Option<bool>   addsigchng;
	Utilities::Option<bool>   allPPCA;
	Utilities::Option<bool>   varplots;
	Utilities::Option<bool>   varvals;

	Utilities::Option<std::string> fn_Tdesign;
	Utilities::Option<std::string> fn_Tcon;
	Utilities::Option<std::string> fn_TconF;
	Utilities::Option<std::string> fn_Sdesign;
	Utilities::Option<std::string> fn_Scon;
	Utilities::Option<std::string> fn_SconF;

  	Utilities::Option<bool>   output_all;
  	Utilities::Option<bool>   output_unmix;
  	Utilities::Option<bool>   output_MMstats;
  	Utilities::Option<bool>   output_pca;
  	Utilities::Option<bool>   output_white;
  	Utilities::Option<bool>   output_origIC;
  	Utilities::Option<bool>   output_mean;

  	Utilities::Option<bool> verbose;
  	Utilities::Option<bool> vers;
  	Utilities::Option<bool> copyright;
  	Utilities::Option<bool> help;
  	Utilities::Option<bool> debug;
	Utilities::Option<bool> readCIFTI;

  	Utilities::Option<std::string> guessfname;
  	Utilities::Option<std::string> paradigmfname;
  	Utilities::Option<std::string> axials_str;

  	Utilities::Option<int>   dummy;
  	Utilities::Option<int>   repeats;
	Utilities::Option<int>   seed;
  	Utilities::Option<float> nlconst1;
  	Utilities::Option<float> nlconst2;
  	Utilities::Option<float> smooth_probmap;
	Utilities::Option<std::string> insta_fn;

  	Utilities::Option<bool> remove_meanvol;
  	Utilities::Option<bool> remove_meantc;
 	Utilities::Option<bool> remove_endslices;
  	Utilities::Option<bool> rescale_nht;

  	Utilities::Option<bool> guess_remderiv;
  	Utilities::Option<bool> temporal;

  	Utilities::Option<float> retryfactor;
  	Utilities::Option<int> econ;

  	void parse_command_line(int argc, char** argv, Utilities::Log& logger,  const std::string &p_version);

  private:
  	MelodicOptions();
  	const MelodicOptions& operator=(MelodicOptions&);
  	MelodicOptions(MelodicOptions&);

  	Utilities::OptionParser options;

  	static MelodicOptions* gopt;

  	void print_usage(int argc, char *argv[]);
  	void print_version();
  	void print_copyright();
  	void status();

  };

  inline MelodicOptions& MelodicOptions::getInstance(){
    if(gopt == NULL)
      gopt = new MelodicOptions();

    return *gopt;
  }

  inline MelodicOptions::MelodicOptions() :
    logdir(std::string("-o,--outdir"), std::string("log.txt"),
           std::string("output directory name"),
           false, Utilities::requires_argument),
    inputfname(std::string("-i,--in"), std::vector<std::string>(),
               std::string("input file names (either single file name or comma-separated list or text file)"),
               true, Utilities::requires_argument),
    outputfname(std::string("-O,--out"), std::string("melodic"),
                std::string("output file name"),
                false, Utilities::requires_argument,false),
    maskfname(std::string("-m,--mask"), std::string(""),
              std::string("file name of mask for thresholding"),
              false, Utilities::requires_argument),
    use_mask(std::string("--nomask"), true,
             std::string("switch off masking"),
             false, Utilities::no_argument),
    update_mask(std::string("--update_mask"), true,
                std::string("switch off mask updating"),
                false, Utilities::no_argument),
    perf_bet(std::string("--nobet"), true,
             std::string("\tswitch off BET"),
             false, Utilities::no_argument),
    threshold(std::string("--bgthreshold"),  0.01,
              std::string("brain / non-brain threshold (only if --nobet selected)\n"),
              false, Utilities::requires_argument),
    pca_dim(std::string("-d,--dim"), 0,
            std::string("dimensionality reduction into #num dimensions (default: automatic estimation)"),
            false, Utilities::requires_argument),
    pca_est(std::string("--dimest"), std::string("lap"),
            std::string("use specific dim. estimation technique: lap, bic, mdl, aic, mean (default: lap)"),
            false, Utilities::requires_argument),
    joined_whiten(std::string("--sep_whiten"), false,
                  std::string("switch on separate whitening"),
                  false, Utilities::no_argument, false),
    joined_vn(std::string("--sep_vn"), true,
              std::string("switch on separate variance nomalisation for each input dataset (off by default)"),
              false, Utilities::no_argument),
    dr_pca(std::string("--mod_pca"), true,
           std::string("switch off modified PCA for concat ICA"),
           false, Utilities::no_argument, false),
    migp(std::string("--disableMigp"), true,
         std::string("switch off MIGP data reduction when using -a concat (full temporal concatenation will be used)"),
         false, Utilities::no_argument),
    migpN(std::string("--migpN"), 0,
          std::string("Number of internal Eigenmaps"),
          false, Utilities::requires_argument),
    migp_shuffle(std::string("--migp_shuffle"), true,
                 std::string("Randomise MIGP file order (default: TRUE)"),
                 false, Utilities::no_argument),
    migp_factor(std::string("--migp_factor"), 2,
                std::string("Internal Factor of mem-threshold relative to number of Eigenmaps (default: 2)"),
                false, Utilities::requires_argument),
    dr(std::string("--dr"), false,
	   std::string("Dual Regression (default: false)"),
	   false, Utilities::no_argument, false),
    dr_out(std::string("--dr_out"), false,
           std::string("Dual Regression output for MIGP/concat ICA"),
           false, Utilities::no_argument, false),
    vn_level(std::string("--vn_level"), float(2.3),
             std::string("variance nomalisation threshold level (Z> value is ignored)"),
             false, Utilities::requires_argument, false),
    numICs(std::string("-n,--numICs"), -1,
           std::string("numer of IC's to extract (for deflation approach)"),
           false, Utilities::requires_argument),
    approach(std::string("-a,--approach"),  std::string("concat"),
             std::string("approach for multi-session/subject data:\n"
                    "\t   concat\ttemporally-concatenated group-ICA using MIGP ( default )\n"
                    "\t   tica  \ttensor-ICA"),
             false, Utilities::requires_argument),

    deflation(std::string("--defl"),false,
              std::string("switch single-subject ICA method from symmetric to deflation"),
              false, Utilities::no_argument,false),
    nonlinearity(std::string("--nl"), std::string("pow3"),
                 std::string("\tnonlinearity: gauss, tanh, pow3 (default), pow4"),
                 false, Utilities::requires_argument),
    varnorm(std::string("--vn,--varnorm"), true,
            std::string("switch off variance normalisation"),
            false, Utilities::no_argument),
    varnorm2(std::string("--vn2"), true,
             std::string("switch off 2nd level variance normalisation"),
             false, Utilities::no_argument, false),
    pbsc(std::string("--pbsc"), false,
         std::string("        switch on conversion to percent BOLD signal change"),
         false, Utilities::no_argument, false),
    pspec(std::string("--pspec"), false,
          std::string("        switch on conversion to powerspectra"),
          false, Utilities::no_argument, false),
    segment(std::string("--covarweight"), std::string(""),
            std::string("voxel-wise weights for the covariance matrix (e.g. segmentation information)"),
            false, Utilities::requires_argument),
    tsmooth(std::string("--spca"),  false,
            std::string("smooth the eigenvectors prior to IC decomposition"),
            false, Utilities::no_argument, false),
    epsilon(std::string("--eps"), 0.00005,
            std::string("minimum error change"),
            false, Utilities::requires_argument),
    epsilonS(std::string("--epsS"), 0.03,
             std::string("minimum error change for rank-1 approximation in TICA"),
             false, Utilities::requires_argument),
    maxNumItt(std::string("--maxit"),  500,
              std::string("\tmaximum number of iterations before restart"),
              false, Utilities::requires_argument),
    maxRestart(std::string("--maxrestart"),  -1,
               std::string("maximum number of restarts\n"),
               false, Utilities::requires_argument),
    rank1interval(std::string("--rank1interval"),  10,
                  std::string("number of iterations between rank-1 approximation (TICA)\n"),
                  false, Utilities::requires_argument,false),
    mmthresh(std::string("--mmthresh"), std::string("0.5"),
             std::string("threshold for Mixture Model based inference"),
             false, Utilities::requires_argument),
    perf_mm(std::string("--no_mm"), true,
            std::string("\tswitch off mixture modelling on IC maps\n "),
            false, Utilities::no_argument),
    ICsfname(std::string("--ICs"), std::string(""),
             std::string("\tinput filename of the IC components file for mixture modelling"),
             false, Utilities::requires_argument),
    filtermix(std::string("--mix"),  std::string(""),
              std::string("\tinput filename of mixing matrix for mixture modelling / filtering"),
              false, Utilities::requires_argument),
    smodename(std::string("--smode"),  std::string(""),
              std::string("\tinput filename of matrix of session modes for report generation"),
              false, Utilities::requires_argument),
    filter(std::string("-f,--filter"),  std::string(""),
           std::string("list of component numbers to remove\n "),
           false, Utilities::requires_argument),
    genreport(std::string("--report"), false,
              std::string("generate Melodic web report"),
              false, Utilities::no_argument),
    guireport(std::string("--guireport"), std::string(""),
              std::string("modify report for GUI use"),
              false, Utilities::requires_argument, false),
    bgimage(std::string("--bgimage"),  std::string(""),
            std::string("specify background image for report (default: mean image)\n "),
            false, Utilities::requires_argument),
    tr(std::string("--tr"),  0.0,
	   std::string("\tTR in seconds"),
	   false, Utilities::requires_argument),
    logPower(std::string("--logPower"),  false,
             std::string("calculate log of power for frequency spectrum\n"),
             false, Utilities::no_argument),
    addsigchng(std::string("--sigchng"),  false,
               std::string("add signal change estimates to report pages\n"),
               false, Utilities::no_argument, false),
    allPPCA(std::string("--allPPCA"),  false,
            std::string("add all PPCA plots\n"),
            false, Utilities::no_argument, false),
    varplots(std::string("--varplots"),  false,
             std::string("add std error envelopes to time course plots\n"),
             false, Utilities::no_argument, false),
    varvals(std::string("--varvals"),  false,
            std::string("add rank1 values after plots\n"),
            false, Utilities::no_argument, false),
    fn_Tdesign(std::string("--Tdes"), std::string(""),
               std::string("        design matrix across time-domain"),
               false, Utilities::requires_argument),
    fn_Tcon(std::string("--Tcon"), std::string(""),
            std::string("        t-contrast matrix across time-domain"),
            false, Utilities::requires_argument),
    fn_TconF(std::string("--Tconf"), std::string(""),
             std::string("        F-contrast matrix across time-domain"),
             false, Utilities::requires_argument, false),
    fn_Sdesign(std::string("--Sdes"), std::string(""),
               std::string("        design matrix across subject-domain"),
               false, Utilities::requires_argument),
    fn_Scon(std::string("--Scon"), std::string(""),
            std::string("        t-contrast matrix across subject-domain"),
            false, Utilities::requires_argument),
    fn_SconF(std::string("--Sconf"), std::string(""),
             std::string("        F-contrast matrix across subject-domain"),
             false, Utilities::requires_argument,false),
    output_all(std::string("--Oall"),  false,
               std::string("        output everything"),
               false, Utilities::no_argument),
    output_unmix(std::string("--Ounmix"),  false,
                 std::string("output unmixing matrix"),
                 false, Utilities::no_argument),
    output_MMstats(std::string("--Ostats"),  false,
                   std::string("output thresholded maps and probability maps"),
                   false, Utilities::no_argument),
    output_pca(std::string("--Opca"),  false,
               std::string("\toutput PCA results"),
               false, Utilities::no_argument),
    output_white(std::string("--Owhite"),  false,
                 std::string("output whitening/dewhitening matrices"),
                 false, Utilities::no_argument),
    output_origIC(std::string("--Oorig"),  false,
                  std::string("\toutput the original ICs"),
                  false, Utilities::no_argument),
    output_mean(std::string("--Omean"),  false,
                std::string("\toutput mean volume\n"),
                false, Utilities::no_argument),
    verbose(std::string("-v,--verbose"), false,
            std::string("switch on diagnostic messages\n\n\n"),
            false, Utilities::no_argument),
    vers(std::string("-V,--version"), false,
         std::string("prints version information"),
         false, Utilities::no_argument),
    copyright(std::string("--copyright"), false,
              std::string("prints copyright information"),
              false, Utilities::no_argument),
    help(std::string("-h,--help"),  false,
         std::string("prints this help message"),
         false, Utilities::no_argument),
    debug(std::string("--debug"),  false,
          std::string("        switch on debug messages"),
          false, Utilities::no_argument),
    readCIFTI(std::string("--CIFTI"),  false,
              std::string("        input/output as CIFTI (warning: auto-dimensionality estimation for CIFTI data is currently inaccurate)"),
              false, Utilities::no_argument),
    guessfname(std::string("--init_ica"), std::string(""),
               std::string("file name of FEAT paradigm file (design.mat) for ICA initialisation"),
               false, Utilities::requires_argument, false),
    paradigmfname(std::string("--init_pca"),  std::string(""),
                  std::string("file name of FEAT paradigm file (design.mat) for PCA initialisation"),
                  false, Utilities::requires_argument, false),
    axials_str(std::string("--report_maps"),  std::string(" -s 2 -A 950 "),
               std::string("control string for spatial map images (see slicer)"),
               false, Utilities::requires_argument),
    dummy(std::string("--dummy"),  0,
          std::string("number of dummy volumes"),
          false, Utilities::requires_argument,false),
    repeats(std::string("--repeats"), 1,
            std::string("number of repeats (multistart)"),
            false, Utilities::requires_argument, false),
    seed(std::string("--seed"), -1,
         std::string("integer seed for random number generator within melodic"),
         false, Utilities::requires_argument, false),
    nlconst1(std::string("--nl1,--nlconst1"),  1.0,
             std::string("nonlinear constant 1"),
             false, Utilities::requires_argument, false),
    nlconst2(std::string("--nl2,--nlconst2"),  1.0,
             std::string("nonlinear constant 2"),
             false, Utilities::requires_argument, false),
    smooth_probmap(std::string("--smooth_pm"),  0.0,
                   std::string("width of smoothing kernel for probability maps"),
                   false, Utilities::requires_argument, false),
    insta_fn(std::string("--insta_fn"), std::string(""),
             std::string(" mask file name for instacorr calculation"),
             false, Utilities::requires_argument, false),
    remove_meanvol(std::string("--keep_meanvol"), true,
                   std::string("do not subtract mean volume"),
                   false, Utilities::no_argument, true),
    remove_meantc(std::string("--remove_meantc"), false,
                  std::string("remove mean time course"),
                  false, Utilities::no_argument, false),
    remove_endslices(std::string("--remEndslices"),  false,
                     std::string("delete end slices (motion correction artefacts)"),
                     false, Utilities::no_argument,false),
    rescale_nht(std::string("--rescale_nht"),  true,
                std::string("switch off map rescaling after mixture-modelling"),
                false, Utilities::no_argument,false),
    guess_remderiv(std::string("--remove_deriv"),  false,
                   std::string("removes every second entry in paradigm file (EV derivatives)"),
                   false, Utilities::no_argument, false),
    temporal(std::string("--temporal"),  false,
             std::string("perform temporal ICA"),
             false, Utilities::no_argument, false),
    retryfactor(std::string("--retryfactor"), float(0.95),
                std::string("multiplicative factor for determining new dim if estimated dim fails to converge"),
                false, Utilities::requires_argument, false),
    econ(std::string("--econ"), 20000,
         std::string("set ctrl parameter for helperfns econ mode"),
         false, Utilities::requires_argument, false),
    options(title, usageexmpl)
  {
    try {
      options.add(logdir);
      options.add(inputfname);
      options.add(outputfname);
      options.add(output_all);
      options.add(maskfname);
      options.add(pca_dim);
      options.add(approach);
      options.add(genreport);
      options.add(readCIFTI);
      options.add(varnorm);
      options.add(verbose);

      options.add(guessfname);
      options.add(use_mask);
      options.add(update_mask);
      options.add(perf_bet);
      options.add(threshold);
      options.add(pca_est);
      options.add(joined_whiten);
      options.add(joined_vn);
      options.add(dr_pca);
      options.add(migp);
      options.add(migpN);
      options.add(migp_shuffle);
      options.add(migp_factor);
      options.add(dr);
      options.add(dr_out);
      options.add(vn_level);
      options.add(numICs);
      options.add(nonlinearity);
      options.add(varnorm2);
      options.add(pbsc);
      options.add(pspec);
      options.add(segment);
      options.add(tsmooth);
      options.add(epsilon);
      options.add(epsilonS);
      options.add(maxNumItt);
      options.add(maxRestart);
      options.add(rank1interval);
      options.add(mmthresh);
      options.add(perf_mm);
      options.add(ICsfname);
      options.add(filtermix);
      options.add(smodename);
      options.add(filter);
      options.add(guireport);
      options.add(bgimage);
      options.add(tr);
      options.add(logPower);
      options.add(addsigchng);
      options.add(allPPCA);
      options.add(varplots);
      options.add(varvals);
      options.add(fn_Tdesign);
      options.add(fn_Tcon);
      options.add(fn_TconF);
      options.add(fn_Sdesign);
      options.add(fn_Scon);
      options.add(fn_SconF);
      options.add(output_unmix);
      options.add(output_MMstats);
      options.add(output_pca);
      options.add(output_white);
      options.add(output_origIC);
      options.add(output_mean);
      options.add(vers);
      options.add(copyright);
      options.add(help);
      options.add(debug);

      options.add(guessfname);
      options.add(paradigmfname);
      options.add(axials_str);
      options.add(dummy);
      options.add(repeats);
      options.add(seed);
      options.add(nlconst1);
      options.add(nlconst2);
      options.add(smooth_probmap);
      options.add(insta_fn);
      options.add(remove_meanvol);
      options.add(remove_meantc);
      options.add(remove_endslices);
      options.add(rescale_nht);
      options.add(guess_remderiv);
      options.add(temporal);
      options.add(retryfactor);
      options.add(econ);
    }
    catch(Utilities::X_OptionError& e) {
      options.usage();
      std::cerr << std::endl << e.what() << std::endl;
    }
    catch(std::exception &e) {
      std::cerr << e.what() << std::endl;
    }

  }
}

#endif

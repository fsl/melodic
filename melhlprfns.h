/*  MELODIC - Multivariate exploratory linear optimized decomposition into
              independent components

    melhlprfns.cc - misc functions

    Christian F. Beckmann, FMRIB Analysis Group

    Copyright (C) 1999-2013 University of Oxford */

/*  CCOPYRIGHT  */

#ifndef __MELODICHLPR_h
#define __MELODICHLPR_h

#include "armawrap/newmat.h"
#include "newimage/newimageall.h"

	#ifdef __APPLE__
	#include <mach/mach.h>
	#define mmsg(msg) { \
	  struct task_basic_info t_info; \
	  mach_msg_type_number_t t_info_count = TASK_BASIC_INFO_COUNT; \
	  if (KERN_SUCCESS == task_info(mach_task_self(), TASK_BASIC_INFO, (task_info_t) &t_info, &t_info_count)) \
		{ \
			cout << " MEM: " << msg << " res: " << t_info.resident_size/1000000 << " virt: " << t_info.virtual_size/1000000 << "\n"; \
			} \
	}
	#else
	#define mmsg(msg) { \
	   cout << msg; \
	}
	#endif

namespace Melodic{

  void update_mask(NEWIMAGE::volume<float>& mask, NEWMAT::Matrix& Data);
  void del_vols(NEWIMAGE::volume4D<float>& in, int howmany);

  NEWMAT::Matrix smoothColumns(const NEWMAT::Matrix& inp);
  NEWMAT::Matrix calc_FFT(const NEWMAT::Matrix& Mat, const bool logpwr = 0);

  NEWMAT::Matrix convert_to_pbsc(NEWMAT::Matrix& Mat);

  NEWMAT::RowVector varnorm(NEWMAT::Matrix& in, int dim = 30, float level = 1.6, int econ = 20000);
       void varnorm(NEWMAT::Matrix& in, const NEWMAT::RowVector& vars);
  NEWMAT::RowVector varnorm(NEWMAT::Matrix& in, NEWMAT::SymmetricMatrix& Corr, int dim = 30, float level = 1.6, int econ = 20000);

  NEWMAT::Matrix SP2(const NEWMAT::Matrix& in, const NEWMAT::Matrix& weights, int econ = 20000);
  void SP3(NEWMAT::Matrix& in, const NEWMAT::Matrix& weights);

  NEWMAT::RowVector Feta(int n1,int n2);
  NEWMAT::RowVector cumsum(const NEWMAT::RowVector& Inp);

  NEWMAT::Matrix corrcoef(const NEWMAT::Matrix& in1, const NEWMAT::Matrix& in2);
  NEWMAT::Matrix corrcoef(const NEWMAT::Matrix& in1, const NEWMAT::Matrix& in2, const NEWMAT::Matrix& part);
  float calc_white(const NEWMAT::Matrix& tmpE, const NEWMAT::RowVector& tmpD, const NEWMAT::RowVector& PercEV, int dim, NEWMAT::Matrix& param, NEWMAT::Matrix& paramS, NEWMAT::Matrix& white, NEWMAT::Matrix& dewhite);
  float calc_white(const NEWMAT::Matrix& tmpE, const NEWMAT::RowVector& tmpD, const NEWMAT::RowVector& PercEV, int dim, NEWMAT::Matrix& white, NEWMAT::Matrix& dewhite);
  void calc_white(const NEWMAT::Matrix& tmpE, const NEWMAT::RowVector& tmpD, int dim, NEWMAT::Matrix& param, NEWMAT::Matrix& paramS, NEWMAT::Matrix& white, NEWMAT::Matrix& dewhite);
  void calc_white(const NEWMAT::Matrix& tmpE, const NEWMAT::RowVector& tmpD, int dim, NEWMAT::Matrix& white, NEWMAT::Matrix& dewhite);
  void calc_white(const NEWMAT::SymmetricMatrix& Corr, int dim, NEWMAT::Matrix& white, NEWMAT::Matrix& dewhite);

  void std_pca(const NEWMAT::Matrix& Mat, NEWMAT::SymmetricMatrix& Corr, NEWMAT::Matrix& evecs, NEWMAT::RowVector& evals, int econ = 20000);
  void std_pca(const NEWMAT::Matrix& Mat, const NEWMAT::Matrix& weights, NEWMAT::SymmetricMatrix& Corr, NEWMAT::Matrix& evecs, NEWMAT::RowVector& evals, int econ = 20000);
  void em_pca(const NEWMAT::Matrix& Mat, NEWMAT::Matrix& evecs, NEWMAT::RowVector& evals, int num_pc = 1, int iter = 20);
  void em_pca(const NEWMAT::Matrix& Mat, NEWMAT::Matrix& guess, NEWMAT::Matrix& evecs, NEWMAT::RowVector& evals, int num_pc = 1, int iter = 20);

  float rankapprox(const NEWMAT::Matrix& Mat, NEWMAT::Matrix& cols, NEWMAT::Matrix& rows, int dim = 1);
  NEWMAT::RowVector krfact(const NEWMAT::Matrix& Mat, NEWMAT::Matrix& cols, NEWMAT::Matrix& rows);
  NEWMAT::RowVector krfact(const NEWMAT::Matrix& Mat, int colnum, NEWMAT::Matrix& cols, NEWMAT::Matrix& rows);
  NEWMAT::Matrix krprod(const NEWMAT::Matrix& cols, const NEWMAT::Matrix& rows);
  NEWMAT::Matrix krapprox(const NEWMAT::Matrix& Mat, int size_col, int dim = 1);

  void adj_eigspec(const NEWMAT::RowVector& in, NEWMAT::RowVector& out1, NEWMAT::RowVector& out2, NEWMAT::RowVector& out3, int& out4, int num_vox, float resels);
  void adj_eigspec(const NEWMAT::RowVector& in, NEWMAT::RowVector& out1, NEWMAT::RowVector& out2);

  int ppca_dim(const NEWMAT::Matrix& in, const NEWMAT::Matrix& weights, NEWMAT::Matrix& PPCA, NEWMAT::RowVector& AdjEV, NEWMAT::RowVector& PercEV, NEWMAT::SymmetricMatrix& Corr, NEWMAT::Matrix& tmpE, NEWMAT::RowVector &tmpD, float resels, std::string which);
  int ppca_dim(const NEWMAT::Matrix& in, const NEWMAT::Matrix& weights, NEWMAT::Matrix& PPCA, NEWMAT::RowVector& AdjEV, NEWMAT::RowVector& PercEV, float resels, std::string which);
  int ppca_dim(const NEWMAT::Matrix& in, const NEWMAT::Matrix& weights, float resels, std::string which);
  NEWMAT::ColumnVector ppca_select(NEWMAT::Matrix& PPCAest, int& dim, int maxEV, std::string which);
  NEWMAT::Matrix ppca_est(const NEWMAT::RowVector& eigenvalues, const int N1, const float N2);
  NEWMAT::Matrix ppca_est(const NEWMAT::RowVector& eigenvalues, const int N);

  NEWMAT::ColumnVector acf(const NEWMAT::ColumnVector& in, int order);
  NEWMAT::ColumnVector pacf(const NEWMAT::ColumnVector& in, int maxorder = 1);
  NEWMAT::Matrix est_ar(const NEWMAT::Matrix& Mat, int maxorder);
  NEWMAT::ColumnVector gen_ar(const NEWMAT::ColumnVector& in, int maxorder = 1);
  NEWMAT::Matrix gen_ar(const NEWMAT::Matrix& in, int maxorder);
  NEWMAT::Matrix gen_arCorr(const NEWMAT::Matrix& in, int maxorder);

	class basicGLM{
		public:

			//constructor
			basicGLM(){}

			//destructor
			~basicGLM(){}

			void olsfit(const NEWMAT::Matrix& data, const NEWMAT::Matrix& design,
				const NEWMAT::Matrix& contrasts, int DOFadjust = -1);

			inline NEWMAT::Matrix& get_t(){return t;}
			inline NEWMAT::Matrix& get_z(){return z;}
			inline NEWMAT::Matrix& get_p(){return p;}
			inline NEWMAT::Matrix& get_f_fmf(){return f_fmf;}
			inline NEWMAT::Matrix& get_pf_fmf(){return pf_fmf;}
			inline NEWMAT::Matrix& get_cbeta(){return cbeta;}
			inline NEWMAT::Matrix& get_beta(){return beta;}
			inline NEWMAT::Matrix& get_varcb(){return varcb;}
			inline NEWMAT::Matrix& get_sigsq(){return sigsq;}
			inline NEWMAT::Matrix& get_residu(){return residu;}
			inline int get_dof(){return dof;}

		private:
			NEWMAT::Matrix beta;
			NEWMAT::Matrix residu;
			NEWMAT::Matrix sigsq;
			NEWMAT::Matrix varcb;
			NEWMAT::Matrix cbeta;
			NEWMAT::Matrix f_fmf, pf_fmf;
			int dof;
			NEWMAT::Matrix t;
			NEWMAT::Matrix z;
			NEWMAT::Matrix p;
  };
//	Matrix glm_ols(const Matrix& dat, const Matrix& design);
}

#endif

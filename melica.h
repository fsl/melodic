/*  MELODIC - Multivariate exploratory linear optimized decomposition into
              independent components

    melica.h - ICA estimation

    Christian F. Beckmann, FMRIB Analysis Group

    Copyright (C) 1999-2013 University of Oxford */

/*  CCOPYRIGHT */

#ifndef __MELODICICA_h
#define __MELODICICA_h
#include "utils/log.h"
#include "armawrap/newmat.h"
#include "melpca.h"
#include "meloptions.h"
#include "meldata.h"
#include "melodic.h"
#include "melreport.h"
#include "ggmix.h"


namespace Melodic{

  class MelodicICA{
  public:
    MelodicICA(MelodicData &pmelodat, MelodicOptions &popts, Utilities::Log &plogger, MelodicReport &preport):
      melodat(pmelodat),
      opts(popts),
      logger(plogger),
      report(preport){}

    void perf_ica(const NEWMAT::Matrix &Data);

    bool no_convergence;

  private:
    MelodicData &melodat;
    MelodicOptions &opts;
    Utilities::Log &logger;
    MelodicReport &report;

    int dim;
    int samples;
    NEWMAT::Matrix redUMM;

    void ica_fastica_symm(const NEWMAT::Matrix &Data);
    void ica_fastica_defl(const NEWMAT::Matrix &Data);
    void ica_maxent(const NEWMAT::Matrix &Data);
    void ica_jade(const NEWMAT::Matrix &Data);
    //void tica();
    //void tica_concat();
    //void parafac();
    NEWMAT::Matrix randm(const int dim1, const int dim2);
    void sort();
    NEWMAT::Matrix sign(const NEWMAT::Matrix &Inp);

  };
}

#endif

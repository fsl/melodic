# A Makefile for MELODIC

include ${FSLCONFDIR}/default.mk

PROJNAME    = melodic
XFILES      = fsl_glm fsl_sbca fsl_mvlm fsl_regfilt \
              fsl_schurprod melodic
TESTXFILES  = test
RUNTCLS     = Melodic
SCRIPTS     = melodicreport dual_regression
SOFILES     = libfsl-ggmix.so

USRINCFLAGS = `pkg-config CiftiLib --cflags`
LIBS        = -lfsl-newimage -lfsl-miscplot -lfsl-miscpic \
              -lfsl-miscmaths -lfsl-NewNifti -lfsl-utils  \
              -lfsl-znz -lfsl-cprob -lgdc -lgd -lpng \
              `pkg-config CiftiLib --libs`

all: ${SOFILES} ${XFILES}

libfsl-ggmix.so: ggmix.o
	$(CXX) ${CXXFLAGS} -shared -o $@ $^ ${LDFLAGS}

melodic:  meloptions.o melhlprfns.o melgmix.o meldata.o melpca.o melica.o melreport.o melodic.o
	$(CXX) ${CXXFLAGS} -o $@ $^ ${LDFLAGS}

test: test.o
	$(CXX) ${CXXFLAGS} -o $@ $^ ${LDFLAGS}

fsl_glm: melhlprfns.o fsl_glm.o
	$(CXX) ${CXXFLAGS} -o $@ $^ ${LDFLAGS}

fsl_sbca: melhlprfns.o fsl_sbca.o
	$(CXX) ${CXXFLAGS} -o $@ $^ ${LDFLAGS}

fsl_schurprod: melhlprfns.o fsl_schurprod.o
	$(CXX) ${CXXFLAGS} -o $@ $^ ${LDFLAGS}

fsl_mvlm: melhlprfns.o fsl_mvlm.o
	$(CXX) ${CXXFLAGS} -o $@ $^ ${LDFLAGS}

fsl_regfilt: melhlprfns.o fsl_regfilt.o
	$(CXX) ${CXXFLAGS} -o $@ $^ ${LDFLAGS}

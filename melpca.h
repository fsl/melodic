 /*  MELODIC - Multivariate exploratory linear optimized decomposition into
              independent components

    melpca.h - PCA and whitening

    Christian F. Beckmann, FMRIB Analysis Group

    Copyright (C) 1999-2013 University of Oxford */

/*  CCOPYRIGHT */

#ifndef __MELODICPCA_h
#define __MELODICPCA_h
#include "utils/log.h"
#include "armawrap/newmat.h"
#include "meloptions.h"
#include "meldata.h"
#include "melodic.h"


namespace Melodic{

  class MelodicReport;

  class MelodicPCA{
  public:
    MelodicPCA(MelodicData &pmelodat, MelodicOptions &popts, Utilities::Log &plogger,
               MelodicReport &preport):
      melodat(pmelodat),
      opts(popts),
      logger(plogger),
      report(preport){}

    void perf_pca(NEWMAT::Matrix& in, NEWMAT::Matrix& weights);
    inline void perf_pca(){
      perf_pca(melodat.get_Data(),melodat.get_RXweight());
    }
    void perf_white();

  private:
    MelodicData &melodat;
    MelodicOptions &opts;
    Utilities::Log &logger;
    __attribute__((unused)) MelodicReport &report;

    int pcadim();
  };
}

#endif

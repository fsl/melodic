/*  MELODIC - Multivariate exploratory linear optimized decomposition into
                 independent components

    melodic.h - main program header

    Christian F. Beckmann and Matthew Webster, FMRIB Analysis Group

    Copyright (C) 1999-2013 University of Oxford */

/*  CCOPYRIGHT */

#ifndef __MELODIC_h
#define __MELODIC_h

#include <strstream>


#ifdef __APPLE__
#include <mach/mach.h>
#define memmsg(msg) { \
  MelodicOptions&opt = MelodicOptions::getInstance(); \
  struct task_basic_info t_info; \
  mach_msg_type_number_t t_info_count = TASK_BASIC_INFO_COUNT; \
  if (KERN_SUCCESS == task_info(mach_task_self(), TASK_BASIC_INFO, (task_info_t) &t_info, &t_info_count)) \
	{ \
		if(opt.debug.value()) {\
		    std::cout << " MEM: " << msg << " res: " << t_info.resident_size/1000000 << " virt: " << t_info.virtual_size/1000000 << "\n"; \
		    std::cout.flush(); \
		 } \
	} \
}
#else
#define memmsg(msg) { \
  MelodicOptions&opt = MelodicOptions::getInstance(); \
  if(opt.debug.value()) {\
		std::cout << msg; \
        std::cout.flush();                      \
  } \
}
#endif



// a simple message macro that takes care of cout and log
#define message(msg) { \
  MelodicOptions& opt = MelodicOptions::getInstance(); \
  if(opt.verbose.value()) \
    { \
      std::cout << msg; \
    } \
  Utilities::Log& logger  =   Utilities::LogSingleton::getInstance();   \
  logger.str() << msg; \
  std::cout.flush(); \
}

#define dbgmsg(msg) { \
  MelodicOptions&opt = MelodicOptions::getInstance(); \
  if(opt.debug.value()) {\
    std::cout << msg; }  \
}

#define outMsize(msg,Mat) { \
  MelodicOptions& opt = MelodicOptions::getInstance();		\
  if(opt.debug.value())						\
    std::cerr << "     " << msg << "  " <<Mat.Nrows() << " x " << Mat.Ncols() << std::endl; \
}

namespace Melodic{

  const std::string version = "3.15";

  // The two strings below specify the title and example usage that is
  // printed out as the help or usage message
  const std::string title=std::string("MELODIC (Version ")+version+")"+
    std::string("\n Multivariate Exploratory Linear Optimised Decomposition into Independent Components\n")+
    std::string("\nAuthor: Christian F. Beckmann \n Copyright(c) 2001-2013 University of Oxford");

  const std::string usageexmpl=std::string(" melodic -i <filename> <options>")+
    std::string("\n \t \t to run melodic")+
	//	   string("\n melodic -i <filename> --mix=melodic_mix")+
	//	   string(" --filter=\"string of component numbers\"")+
	//	   string("\n \t \t to remove estimated ICs from input")+
    std::string("\n melodic -i <filename> --ICs=melodic_IC")+
    std::string(" --mix=melodic_mix <options>")+
    std::string("\n \t \t to run Mixture Model based inference on estimated ICs")+
    std::string("\n melodic --help ");
}

#endif
